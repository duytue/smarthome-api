import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import DeviceCard from "./DeviceCard";

class Dashboard extends Component {

  render() {
    return (
      <div className="container">
        <div className="row card-container">
          <div className="col s12 m6 l4"><DeviceCard name="Balcony Moisture" imgSrc="images/moisture.jpg" type="mois"/></div>
          <div className="col s12 m6 l4"><DeviceCard name="Outdoor Temparature" imgSrc="images/temp.jpg" type="temp"/></div>
          <div className="col s12 m6 l4"><DeviceCard /></div>
        </div>

        <li className="divider"></li>
        <div className="row control-panel">

        </div>

      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);
