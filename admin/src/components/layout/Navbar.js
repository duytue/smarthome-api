import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import M from 'materialize-css';

import { logoutUser } from "../../actions/authActions";
import TabNav from "./TabNav";

class Navbar extends Component {

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  componentDidMount() {
    let elems = document.querySelectorAll('.dropdown-trigger');
    M.Dropdown.init(elems, { inDuration: 300, outDuration: 225 });
  };

  render() {

    const { isAuthenticated, user } = this.props.auth;

    return (
      <div className="navbar-extended">

        <ul id="dropdown1" className="dropdown-content">
          <li><a href="#!">one</a></li>
          <li><a href="#!">two</a></li>
          <li className="divider"></li>
          <li><a href="#!" onClick={this.onLogoutClick}>Logout</a></li>
        </ul>

        <nav className="z-depth-0">
          <div className="nav-wrapper white">
            <Link
              to="/"
              style={{
                fontFamily: "monospace",
                paddingLeft: "10px"
              }}
              className="col s5 brand-logo left black-text"
            >
              <i className="material-icons">code</i>
              S.M.A.R.T HOME
            </Link>
            <ul className="right hide-on-med-and-down">
              {isAuthenticated ? <li><a className="dropdown-trigger" href="#!" data-target="dropdown1">Hello, {user.name}<i className="material-icons right">arrow_drop_down</i></a></li> : null}
            </ul>
          </div>

          {isAuthenticated ? <TabNav /> : null}

        </nav>
      </div>
    );
  }
}

Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logoutUser })(Navbar);
