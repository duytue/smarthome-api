import React, { Component } from "react";

class DeviceCard extends Component {

    renderInfo(type) {
        switch (type) {
            case "mois":
                return <div className="card-content white-text">
                    <p>Tree 1: </p>
                    <p>Tree 2: </p>
                    <p>Tree 3: </p>
                </div>
            case "temp":
                return <div className="card-content white-text">
                    <p>Current temparature: </p>
                </div>
            default:
                break;
        }
    }

    render() {
        return (
            <div className="card blue darken-3 small z-depth-3">

                <div className="card-image white-text">
                    <img src={this.props.imgSrc} alt=""/>
                    <span className="card-title" style={{ color: "white" }}>{this.props.name}</span>
                </div>

                {this.renderInfo(this.props.type)}

            </div>
        );
    }
}

export default DeviceCard;