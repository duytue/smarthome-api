import React, { Component } from "react";

class TabNav extends Component {
    render() {
        return (
            <div className="nav-content">
                <ul className="tabs z-depth-0">
                    <li className="tab"><a href="#test1">Floor 1st</a></li>
                    <li className="tab"><a className="active" href="#test2">Floor 2nd</a></li>
                </ul>
            </div>
        )
    }
}

export default TabNav;